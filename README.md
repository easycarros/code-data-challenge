# Code Data Challenge

Desafio Easy Carros baseado em dados.


## Introdução

Você está participando do processo para integrar o time de Produto e Tecnologia da [Easy Carros](https://easycarros.com/).

Este desafio tem como objetivo avaliar suas *skills* na criação de uma solução baseada em dados para um problema do mundo real.


## O que é a Easy Carros?

A Easy Carros surgiu como uma plataforma de marketplace para serviços automotivos.

Hoje somos um hub que oferece diversos serviços para o mercado de mobilidade de logística.


## O desafio

Você tem uma fonte de dados (multas.sql e multas.json - os dados são os mesmos, só muda o formato do arquivo para 
conveniência) com aproximadamente 25 mil registros de multa reais (anonimizados), com diversas informações disponíveis.

Sua missão é sugerir pelo menos três possíveis soluções/implementações baseadas nessa massa de dados em 
anexo e implementar ao menos uma dessas soluções.

As soluções devem passar por algo que obtenha inteligência dos dados fornecidos (por exemplo, o veículo X foi quem mais recebeu multas do tipo Y num determinado período).

_Pode ser que os dados apresentados precisem de alguma normalização ou higienização. Faça uma análise e decida se é realmente necessário._


**A implementação final pode ser:**
- Uma API/endpoint que receba algum input (seja placa do veículo, UF, cidade, orgão, código da multa, etc - a seu 
critério) e retorne informações/insights relativos; 
- Ou ainda pode ser uma interface que mostre gráficos ou relatórios baseados nos dados;
- Ou um algoritmo ML treinado com esses dados que retorne insights;
- Ou tudo junto :P
- Ou a forma que preferir. Seja criativo!


## As regras do jogo

- 📚Use a linguagem, tecnologia, plataforma ou qualquer outra opção com a qual você se sente mais confortável.
- Assuma que todos os dados são válidos (não há necessidade de uma camada de validação).
- Envie sua solução para um repositório público para leitura (Github, Bitbucket, Gitlab, etc.).
- 🗒Crie um arquivo `README` na raiz do projeto com instruções detalhadas de como executar e testar a solução.

### Bônus

- 📦 Use Docker para empacotar todas as dependências em um único lugar.


### Como vou ser avaliado?

Vamos analisar sua solução com respeito a:

- Escopo da solução
    - Se a solução faz sentido para os dados apresentados.
    - Explicação sobre o que foi feito, como foi feito e porque foi feito.
    
    
###### Caso implemente algum código:
- Qualidadade de código e Boas práticas
    - Keep it simple! (KISS)
    - Separation of Concerns (SoC)
    - Design patterns (se houver necessidade)
    - Clean code
    - Code styling (identação/lint) 🙏
    - Documentação

O que **NÃO** vamos analisar:

- Performance
- Escolha da tecnologia A em vez da B
- Forma que a solução foi implementada


## Para onde enviar seu repositório

Envie um email para `tech@easycarros.com` com o assunto `Desafio Code Data Challenge - [SEU NOME]` contendo o link para o repositório que você criou.
